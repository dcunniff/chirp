# README #

###Chirp###
Chirp is an Twitter clone written using ASP.NET 5.

* You can view this site at:
    * http://chirpchirp.azurewebsites.net/
* Since signing up isn't implemented yet, the only user is a fake user (fake email)
    * You can find it in ChirpContextSeedData.cs
### Necessary Downloads ###

* Visual Studio 2015
	* https://www.visualstudio.com/
* ASP.NET 5 RC 1
	* https://get.asp.net/

### Configuration ###
You will need to change the config.json file to point to a real postgreSQL database. Use the template provided to get the format correct.
### Other ###
* This project would not be possible without the below tutorial by Sean Wildermuth:
    * https://app.pluralsight.com/library/courses/aspdotnet-5-ef7-bootstrap-angular-web-app/table-of-contents (Pluralsight Subscription Required)
* Additionally, the name and project idea comes from the tutorial below:
    * https://www.youtube.com/watch?v=Jh0er2pRcq8

### Who do I talk to? ###

Steve Fallon (sfallon92@gmail.com)